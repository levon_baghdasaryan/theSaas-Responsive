$(document).ready(function(){
	
  //Menu sliding process
  if ($(window).width() <= 600) {
  	  $("nav  ul  li").click(function() {
		$(this).children("ul").slideToggle(500);
		$(this).toggleClass("active");
	});
  	  $("nav ul li:first-child a").removeClass("active");
  }
  $(".cross-mark").click(function() {
  		$("nav").css("display", "none");
  });
   $(".menu-hamburger").click(function() {
  		$("nav").css("display", "block");
  });


//***Gallery appearing***
//Works for desktop and mobile

//Moves every subsequent gallery-section  70px lower than the previous one
$(".gallery-section").each(function(i) {
  if ($(window).width() <= 600) {
    $(this).css("top", "90px");
  } else { 
    i++;
    if ( i > 6) {
      i -= 6;
    } else if (i >= 4) {
      i -= 3;
    }
  $(this).css("top", i*70 + "px");
  }
});


//Pictures appear on scroll
$(window).on("scroll", function() {
  $(".gallery-section").each(function(i) {
    var gal; 
    if ($(window).width() <= 600) {
        gal = $(this).offset().top + 50;
        if($(window).height() + $(window).scrollTop() >= gal) {
            $(this).animate({top: "0", opacity: "1"}, 600);
        }
    } else { 
      i++;
      if ( i > 6) {
          i -= 6;
      } else if (i >= 4) {
          i -= 3;
      }  
      gal = $(this).offset().top + 120 - (i * 70);
      if($(window).height() + $(window).scrollTop() >= gal) {
      $(this).animate({top: "0", opacity: "1"}, 1000);
      }
    }      
  });
});
//end of gallery appearing

}); //end of document ready